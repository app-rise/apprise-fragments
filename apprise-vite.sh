
#!/usr/bin/bash

CMD=$1||"start"

installdir=$( cd "$( dirname "$(realpath "$0")" )" && pwd )

shift

case $CMD in

  "start")
		fragments link && fragments postprocess && apprise watch-fragments && vite dev $@
    ;;

  "build")
    fragments link && fragments postprocess && vite build $@
    ;;

	"preinstall")
    fragments link
    ;;

  "serve")
    vite preview
    ;;

	"link-fragments")
    fragments link
    ;;

  "clone-fragments")
		fragments clone
    ;;

  "watch-fragments")
		fragments watch &
    ;;

  "process-fragments")
		fragments link && fragments postprocess
    ;;

  *)
    echo "command not supported!"
    ;;
esac

