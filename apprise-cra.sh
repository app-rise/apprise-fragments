
#!/usr/bin/bash

CMD=$1||"start"

installdir=$( cd "$( dirname "$(realpath "$0")" )" && pwd )

case $CMD in

  "start")
		fragments link && fragments postprocess && apprise watch-fragments && react-app-rewired $CMD --config-overrides "$installdir/config-overrides.js"
    ;;

  "build")
    fragments link && fragments postprocess && CI=false react-app-rewired $CMD --config-overrides "$installdir/config-overrides.js"
    ;;

	"preinstall")
    fragments link
    ;;

  "lint-fragments")
    eslint src/lib/* --max-warnings=0 --ext  .ts,.tsx 
    ;;

	"link-fragments")
    fragments link
    ;;

  "clone-fragments")
		fragments clone
    ;;

  "watch-fragments")
		fragments watch &
    ;;

  "process-fragments")
		fragments link && fragments postprocess
    ;;

  *)
    echo "command not supported!"
    ;;
esac

