#!/usr/bin/env node


/*

fragments.js: https://gitlab.com/app-rise/apprise-tracker/-/issues/9

*/

var fs = require('fs')
var paths = require('path')
var execHome = process.cwd()
var pkg = require(execHome+"/package.json")
var execSync = require('child_process').execSync



var home = execHome

const green = "\x1b[32m"
const cyan = "\x1b[36m"
const red = "\x1b[31m"
const white = "\x1b[0m"

const defaultRef = 'master'

const codeType = 'code'
const defaultCodeMountPoint = 'src/lib'
const defaultCodeMountPath = 'lib'

const resourceType = 'resources'
const defaultResourceMountPath = 'resources'
const localResourceMountPath = 'resources'
const defaultResourceMountPoint = 'resources/lib'
const defaultCloneDir = "/tmp"

const imageTargetFolder = `${home}/public/images`
const localeTargetFolder = `${home}/public/locale`
const translationSnippet = '_translation.json'


function clone(root) {

	const dir = paths.resolve(root || defaultCloneDir)

	sourceDependencies().forEach(dep => {

		// const mounts = cfg.mounts
		const checkout = `${dir}/${dep.name}`

		if (!fs.existsSync(checkout) || dir == defaultCloneDir) {

			//console.log(`cleaning for '${dep.name}'...`)
			if (dir === defaultCloneDir)
				exec(`rm -rf ${checkout}`).in(dir)

			console.log(`cloning ${green}${dep.name}${white} in ${cyan}${dir}${white}...`)
			//console.log(`git clone ${cfg.origin} ${dep} in ${tmp}`)
			exec(`git clone --quiet --branch ${dep.ref} ${dep.origin} ${dep.name}`).in(dir)

		}

	})


}

function postprocess() {
	processImages()
	processLocales()
}


function watch() {

	watchLocales()
	watchImages()



}


//Iterates over all the fragments in package.json and tries to link them.
function link() {

	sourceDependencies().forEach(dep => {

		const possiblePath = getAllDependenciesPaths().find(path => fs.existsSync(`${path}${dep.name}`))

		if (possiblePath !== undefined)
			linkdep(dep, `${possiblePath}${dep.name}`)
		else
			console.error(`${red}fragment ${dep.name} not found in workspace.${white}`)

	})
}



module.exports = {
	codeType,
	resourceType,
	defaultCodeMountPoint,
	defaultResourceMountPoint,
	sourceDependencies,
	mountsOf,
	link,
	postprocess,
	clone,
	watch
}



// if there's an argument, assumes it's an exported function and tries to execute it.
// supports use from package scripts.

var fun = process.argv[2]
var args = process.argv.slice(3)
if (fun) {
	if (fun in module.exports)
		module.exports[fun](...args)
	else if (fun && !fun.startsWith('--'))
		console.warn(`unknown fun: "${fun}"`)
}



// helpers

function getAllDependenciesPaths() {
	const defaultSearchFolders = [...getAllParentPaths(home), '/tmp/']
	if (process.env.APPRISE_LIBRARY_HOME !== undefined)
		return [
			...process.env.APPRISE_LIBRARY_HOME.split(';').filter(path => fs.existsSync(path)).map(path => `${paths.resolve(path)}${paths.sep}`),
			...defaultSearchFolders
		]
	else
		return defaultSearchFolders
}

function getAllParentPaths(folder) {

	var folders = []
	let cur = paths.resolve(folder)
	while (!folders.includes(paths.resolve(cur, '..'))) {
		cur = paths.resolve(cur, '..')
		folders.push(cur)
	}
	return folders.map(folder => folder.endsWith(paths.sep) ? folder : `${folder}${paths.sep}`)

}


function sourceDependencies() {

    const defaulted = Object.keys(pkg.srcDependencies).map(key => {

		const d = pkg.srcDependencies[key]

		const dep = typeof d === "string" ? { origin: d } : d

		dep.name = key

		dep.ref = dep.ref || defaultRef

		dep.mounts = (dep.mounts || [codeType, resourceType]).map(m => {

			const mount = typeof m === "string" ? { type: m } : m

			mount.type = mount.type || codeType

			if (mount.type === codeType) {

				mount.path = mount.path || `/${defaultCodeMountPath}`
				mount.mountpoint = mount.mountpoint || `/${defaultCodeMountPoint}/${key}`
			}

			if (mount.type === resourceType) {

				mount.path = mount.path || `/${defaultResourceMountPath}`
				mount.mountpoint = mount.mountpoint || `/${defaultResourceMountPoint}/${key}`
			}

			return mount

		})

		return dep


	})

	return defaulted
}


function mountsOf(type) {

	return sourceDependencies().reduce((acc, dep) => ([...acc, ...(dep.mounts || [])]), []).filter(mount => mount.type === type)

}



// Links all the mounts for a specific fragment
function linkdep(dep, orig) {

	if (orig === undefined || orig.trim() === "") {
		console.error(`${red}link what? no path provided.${white}`)
		process.exit(1)
	}

	const resolved = paths.resolve(orig)

	const mounts = dep.mounts

	mounts.forEach(mount => {

		const targetDir = `${home}${paths.dirname(mount.mountpoint)}`

		if (!fs.existsSync(targetDir))
			exec(`mkdir -p ${targetDir}`).in(home)

		const target = `${home}${mount.mountpoint}`

		const src = paths.resolve(`${resolved}${mount.path}`)

		if (!fs.existsSync(target) || !fs.lstatSync(target).isSymbolicLink()) {
			console.log(`mounting ${cyan}${mount.type}${white} for ${green}${dep.name}${white}...`)
			exec(`ln -fns ${src} ${target}`).in(home)
		}

		// //If type code we try to link also the package.json if it exists
		// if (mount.type === codeType) {
		// 	const dependencyFolder = `${home}/packages/${package}`
		// 	const sourcePackageFile = `${paths.resolve(`${resolved}${mount.path}`)}/package.json`
		// 	const targeyPackageFile = `${dependencyFolder}/package.json`
		// 	if (fs.existsSync(sourcePackageFile)) {
		// 		if (!fs.existsSync(dependencyFolder))
		// 			exec(`mkdir -p ${dependencyFolder}`).in(home)
		// 		if (!fs.existsSync(targeyPackageFile)) {
		// 			console.log(`linking ${sourcePackageFile} to ${targeyPackageFile}`)
		// 			exec(`ln -s ${sourcePackageFile} ${targeyPackageFile}`).in(home)
		// 		} else {
		// 			console.log(`${targeyPackageFile} already exists`)
		// 		}
		// 	}
		// }
	})
}


// copy images from source folder to public.
function processImages() {

	const mountsAndLocal = [...mountsOf(resourceType).map(m => m.mountpoint), localResourceMountPath]

	mountsAndLocal.forEach(path => {

		const source = `${home}/${path}`

		const sourceFolder = `${source}/images`

		if (!fs.existsSync(`${imageTargetFolder}`))
			exec(`mkdir -p ${imageTargetFolder}`).in(home)

		if (fs.existsSync(`${sourceFolder}`))
			exec(`cp -r ${sourceFolder}/* ${imageTargetFolder} 2>/dev/null || true`).in(home)   // silent on empty dir.

	})
}

// Watch any image folder for any resource dependency for all srcDependency in package.json for changes.
// If a change is recognized processImages is invoked
function watchImages() {

	var watch = require('node-watch')

	const mountsAndLocal = [...mountsOf(resourceType).map(m => m.mountpoint), localResourceMountPath]

	mountsAndLocal.forEach(path => {

		const source = `${home}/${path}`

		const imagesFolder = `${source}/images`

		if (fs.existsSync(imagesFolder)) {

			//console.log("Watching images folder: ", imagesFolder)

			watch(imagesFolder, {}, function (_, __) {
				processImages()
			})
		}
	})
}

// Merge all locales by language for any resource dependency on all srcDependency in the package.json and copies them to their targets by language.
function processLocales() {

	const merge = require('deepmerge')

	if (!fs.existsSync(`${localeTargetFolder}`))
		exec(`mkdir -p ${localeTargetFolder}`).in(home)

	var localeAccumulator = {}

	const mountsAndLocal = [...mountsOf(resourceType).map(m => m.mountpoint), localResourceMountPath]

	mountsAndLocal.forEach(path => {

		const source = `${home}/${path}`

		const localeFolder = `${source}/locale`

		if (fs.existsSync(localeFolder))
			fs.readdirSync(localeFolder).filter(file => file.includes(translationSnippet)).forEach(file => {

				const lang = file.replace(translationSnippet, '').toLocaleLowerCase()

				try {

					const contents = { [lang]: JSON.parse(fs.readFileSync(`${localeFolder}/${file}`)) }

					localeAccumulator = merge(localeAccumulator, contents)

				} catch (e) {

					console.log("cannot parse translation file", e)

				}
			})
	})

	Object.keys(localeAccumulator).forEach(lang => {

		const targetFile = `${localeTargetFolder}/${lang}${translationSnippet}`

		try {

			fs.writeFileSync(targetFile, JSON.stringify(localeAccumulator[lang]))

		}
		catch (e) {
			console.log("cannot parse translation file", e)
		}
	})
}

// Watch any locale folder for any resource dependency for all srcDependency in package.json for changes.
// If a change is recognized processLocales() is invoked
function watchLocales() {

	var watch = require('node-watch')

	const mountsAndLocal = [...mountsOf(resourceType).map(m => m.mountpoint),localResourceMountPath]

	mountsAndLocal.forEach(path => {

		const source = `${home}/${path}`

		const localeFolder = `${source}/locale`

		//console.log("Watching locale folder: ", localeFolder)

		if (fs.existsSync(localeFolder))
		watch(localeFolder, {}, function (_, __) {

			//console.log("change detected!",localeFolder)
			processLocales()
		})
	})
}






// Execute a shell command
function exec(cmd) {

	return {

		in: path => {

			cmd = `echo $PWD && cd ${path || home} && ${cmd}`

			execSync(cmd, function (err, stdout, stderr) {

				if (err)
					console.log(stderr)

				else
					console.log(stdout)

			})

		}
	}

}