

const { override, babelInclude, addWebpackAlias, removeModuleScopePlugin } = require('customize-cra')

const path = require('path')
const fs = require('fs')
const { sourceDependencies, codeType } = require("./index.js")

const aliases = sourceDependencies().map(d => [d, d.mounts.find(m => m.type === codeType)])
  .filter(([_, m]) => !!m)
  .reduce((acc, [d, mount]) => ({ ...acc, [d.name]: fs.realpathSync(mount.mountpoint.substring(1)) }), {})

const paths = Object.keys(aliases).map(p => [path.resolve('/'), aliases[p]])

module.exports = override(
  removeModuleScopePlugin(),
  babelInclude(paths),
  addWebpackAlias(aliases)
)

