# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [2.1.0](https://gitlab.com/app-rise/apprise-fragments/compare/v1.1.12...v2.1.0) (2024-08-10)


### Features

* passes rest of params to vite ([c3f3b2d](https://gitlab.com/app-rise/apprise-fragments/commit/c3f3b2dab9d5d8a9e65a44508c7ddcea73bf6f1b))


### Bug Fixes

* **vite:** yarn serve doesn't preview ([ec72e21](https://gitlab.com/app-rise/apprise-fragments/commit/ec72e218a4f7662c29898f56b9c1b419dcb9bf63))

### [1.1.12](https://gitlab.com/app-rise/apprise-fragments/compare/v1.1.11...v1.1.12) (2022-03-28)


### Bug Fixes

* no attempt to 'source' in apprise.sh ([1f77f3f](https://gitlab.com/app-rise/apprise-fragments/commit/1f77f3f2a2c9c741f249f72d6d82ee4a63111e0d))

### [1.1.11](https://gitlab.com/app-rise/apprise-fragments/compare/v1.1.10...v1.1.11) (2022-03-14)


### Bug Fixes

* realpath replaces readlink for better interop ([e441f3f](https://gitlab.com/app-rise/apprise-fragments/commit/e441f3f3f2149ee01c2ccb0fc58a6c35167a7267))

### [1.1.10](https://gitlab.com/app-rise/apprise-fragments/compare/v1.1.9...v1.1.10) (2022-03-06)


### Bug Fixes

* apprise supports lint-fragments ([cd1af7f](https://gitlab.com/app-rise/apprise-fragments/commit/cd1af7fa691200b50ffa20e488c4b13a7b600136))

### [1.1.9](https://gitlab.com/app-rise/apprise-fragments/compare/v1.1.8...v1.1.9) (2022-03-04)


### Bug Fixes

* regression in cp images ([05cd631](https://gitlab.com/app-rise/apprise-fragments/commit/05cd631ef1993d7ff72e39dead94d62ee1e76b5e))

### [1.1.8](https://gitlab.com/app-rise/apprise-fragments/compare/v1.1.7...v1.1.8) (2022-03-03)

### [1.1.7](https://gitlab.com/app-rise/apprise-fragments/compare/v1.1.3...v1.1.7) (2022-03-03)


### Bug Fixes

* adding node-watch as a dependency ([587f67a](https://gitlab.com/app-rise/apprise-fragments/commit/587f67a7caf79453aed4439c4573d4b4868082b7))

### [1.1.5](https://gitlab.com/app-rise/apprise-fragments/compare/v1.1.4...v1.1.5) (2022-03-01)

### [1.1.4](https://gitlab.com/app-rise/apprise-fragments/compare/v1.1.3...v1.1.4) (2022-03-01)


### Bug Fixes

* adding node-watch as a dependency ([587f67a](https://gitlab.com/app-rise/apprise-fragments/commit/587f67a7caf79453aed4439c4573d4b4868082b7))

### [1.1.3](https://gitlab.com/app-rise/apprise-fragments/compare/v1.1.2...v1.1.3) (2022-02-25)

### [1.1.2](https://gitlab.com/app-rise/apprise-fragments/compare/v1.1.1...v1.1.2) (2022-02-25)

### [1.1.1](https://gitlab.com/app-rise/apprise-fragments/compare/v1.1.0...v1.1.1) (2022-02-25)

## 1.1.0 (2022-02-25)


### Features

* Now works as a binary ([1425f8d](https://gitlab.com/app-rise/apprise-fragments/commit/1425f8d8a7e4342506158a72f20043fb9b534024))
