# Apprise fragments binary

## How to use in applications

 - Install this as a global dependency:

    ```bash
      yarn global add git+ssh://git@gitlab.com:app-rise/apprise-fragments
    ```

 - In in your repo, use the following scripts in your `package.json`:

    ```Json
    "workspaces": [
        "src/lib/*"
    ],
    "srcDependencies": {
        "lib1": "git@lib1.git",
        "lib2": "git@lib2.git"
    },
    "scripts": {
        "preinstall": "apprise preinstall",
        "start": "apprise start",
        "build": "apprise build",
        "clone-fragments": "apprise clone-fragments",
        "lint-fragments": "apprise lint-fragments"
    },
    ```

- To integrate with `Vite` replace `apprise` above with `apprise-vite`.

### How to release a new version

The release of the constructs in `npm` is automated in the CD/CI pipeline. So you will simply need to release your code in the `main` branch of the repository to see your package available on `npm`. 
Remember to upgrade the version of the package as following:
- Commit your changes, each commit in the repository should follow the convention: [https://www.conventionalcommits.org/en/v1.0.0/](https://www.conventionalcommits.org/en/v1.0.0/)
- Run `yarn release` to auto generate the `CHANGELOG.md` file and upgrade the constructs version.
- Pull request the code to the master branch of the repo

If you try to release a version already published (i.e. you forget to upgrade the package version in the mentioned file) the release will fail. 
Check out the CD/CI pipeline for additional information.